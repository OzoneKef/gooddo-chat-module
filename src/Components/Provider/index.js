import React from 'react';
import { io as SocketIO } from 'socket.io-client';

export const ChatContext = React.createContext('');

export const ChatUsersContext = React.createContext({});

export const ChatListContext = React.createContext([]);

export function useChat() {
	return {
		chats: React.useContext(ChatListContext),
		users: React.useContext(ChatUsersContext),
	}
}

export function Provider(props) {
	const [socket, setSocket] = React.useState();

	React.useEffect(() => {
		setSocket(() => SocketIO(props.url, {
			autoConnect: false,
			reconnectionDelayMax: 10000,
			auth: {
				token: props.token,
			},
		}));
	}, [props.url, props.token]);

	React.useEffect(() => {
		if (socket) {
			socket.connect();
			socket.on('connect', () => {
				console.log('ESTABLISHED')
			});
		}
	}, [socket]);

	const ChatContextValue = React.useMemo(() => props.url, [props.url]);
	const [ChatUsersContextValue, setChatUsersContextValue] = React.useState({});
	const [ChatListContextValue, setChatListContextValue] = React.useState([]);

	return <ChatContext.Provider value={ChatContextValue}>
		<ChatUsersContext.Provider value={ChatUsersContextValue}>
			<ChatListContext.Provider value={ChatListContextValue}>
				{props.children}
			</ChatListContext.Provider>
		</ChatUsersContext.Provider>
	</ChatContext.Provider>
}