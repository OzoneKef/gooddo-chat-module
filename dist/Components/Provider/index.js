"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.ChatUsersContext = exports.ChatListContext = exports.ChatContext = void 0;
exports.Provider = Provider;
exports.useChat = useChat;

require("core-js/modules/web.dom-collections.iterator.js");

var _react = _interopRequireDefault(require("react"));

var _socket = require("socket.io-client");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const ChatContext = /*#__PURE__*/_react.default.createContext('');

exports.ChatContext = ChatContext;

const ChatUsersContext = /*#__PURE__*/_react.default.createContext({});

exports.ChatUsersContext = ChatUsersContext;

const ChatListContext = /*#__PURE__*/_react.default.createContext([]);

exports.ChatListContext = ChatListContext;

function useChat() {
  return {
    chats: _react.default.useContext(ChatListContext),
    users: _react.default.useContext(ChatUsersContext)
  };
}

function Provider(props) {
  const [socket, setSocket] = _react.default.useState();

  _react.default.useEffect(() => {
    setSocket(() => (0, _socket.io)(props.url, {
      autoConnect: false,
      reconnectionDelayMax: 10000,
      auth: {
        token: props.token
      }
    }));
  }, [props.url, props.token]);

  _react.default.useEffect(() => {
    if (socket) {
      socket.connect();
      socket.on('connect', () => {
        console.log('ESTABLISHED');
      });
    }
  }, [socket]);

  const ChatContextValue = _react.default.useMemo(() => props.url, [props.url]);

  const [ChatUsersContextValue, setChatUsersContextValue] = _react.default.useState({});

  const [ChatListContextValue, setChatListContextValue] = _react.default.useState([]);

  return /*#__PURE__*/_react.default.createElement(ChatContext.Provider, {
    value: ChatContextValue
  }, /*#__PURE__*/_react.default.createElement(ChatUsersContext.Provider, {
    value: ChatUsersContextValue
  }, /*#__PURE__*/_react.default.createElement(ChatListContext.Provider, {
    value: ChatListContextValue
  }, props.children)));
}